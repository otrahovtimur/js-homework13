// ТЕОРІЯ
/*

Метод event.preventDefault() реалізує відміну дії, яка задана обєкту в браузері.

`event.preventDefault()` скасовує стандартну дію браузера при події.

Події документа:
DOMContentLoaded - коли HTML розмітку завантажено й оброблено, DOM документ повністю побудований та доступний.
Подія браузера:
onload - загрузка стороннього ресурса.
onerror - вивід помилки при загрузці стороннього ресурсу.
*/



// ПРАКТИКА

const button = document.querySelector('.tabs');
const textContent = document.querySelector('.tabs-content');

button.addEventListener("click", function(event) {
  let target = event.target;
  if (!target) return;

  let btn = target.dataset.active;

  target.classList.toggle('highlight');

  for(let text of textContent.children){  
    if(btn == text.dataset.active){
      text.classList.toggle('open');
    }}
});



